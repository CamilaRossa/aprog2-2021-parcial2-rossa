﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Envio
    {
        public string NroEnvioAleatorio { get; set; }
        public string Destinatario { get; set; }
        public string Repartidor { get; set; }
        public DateTime FechaEstimadaEntrega { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaEntrega { get; set; }

        public enum EstadoEnvio {pendiente, asignadoARepartidor, enCamino, entregado }

        
    }
}

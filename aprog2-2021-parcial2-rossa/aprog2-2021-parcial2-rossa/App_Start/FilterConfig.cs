﻿using System.Web;
using System.Web.Mvc;

namespace aprog2_2021_parcial2_rossa
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

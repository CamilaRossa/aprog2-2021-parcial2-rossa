﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace aprog2_2021_parcial2_rossa.Controllers
{
    public class ObjetoController : Controller
    {

        // GET: Objeto
        public ActionResult Index()
        {
            return View();
        }

        // GET: Objeto/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Objeto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Objeto/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Objeto/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Objeto/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Objeto/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Objeto/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
